import pandas as pd
from reportlab.pdfgen import canvas
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4,inch
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Spacer
import random

df = pd.read_csv('AMR01H-1.csv',sep=';',converters={'SSR': lambda x: str(x)})

doc = SimpleDocTemplate("arrival.pdf",showBoundary=1,leftMargin=0, rightMargin=0, topMargin=0, bottomMargin=0, pagesize=A4)
doc1 = SimpleDocTemplate("departure.pdf",showBoundary=1,leftMargin=0, rightMargin=0, topMargin=0, bottomMargin=0, pagesize=A4)

elements = []
elements1 = []

#df.fillna(0,inplace = True)

for i in range(11):
  if df.ADES[i] == 'BIKF':
    CALLS = df.CALLS[i]
    SSR = (df.SSR[i])
    TYPE = (df.TYPE[i])
    WTC = (df.WTC[i])

    mach_list = ['M079', 'M080' ,'M081' ,'M082' ]
    mach_item = random.choice(mach_list)

    oldSTAR = (df.STAR[i])
    listSTAR = list(oldSTAR) 
    listSTAR.append(listSTAR.pop(listSTAR.index(listSTAR[0])))
    listSTAR.append(listSTAR.pop(listSTAR.index(listSTAR[0])))
    s = ''
    newSTAR = (s.join(listSTAR))
    slicedSTAR = listSTAR[0:5]
    newslicedSTAR = (s.join(slicedSTAR))

    listOfSTAR1 = ['DEVUD3N' , 'NASBU3N' , 'ASRUN2N' , 'BASLU2N']
    listOfSTAR2 = ['INGAN3N','GIRUG2N','ELDIS2N','BIRNA1N']
    listOfSTAR3 = ['DEVUD2K','NASBU2K','ASRUN2K','BASLU2K']
    listOfSTAR4 = ['INGAN2K','GIRUG2K', 'ELDIS2K','BIRNA2K']

    if newSTAR in listOfSTAR1:
      ultimateSTAR = (newslicedSTAR + "\n" + newSTAR + "\n" + 'DEXON')
    elif newSTAR in listOfSTAR2:
      ultimateSTAR = (newslicedSTAR + "\n" + newSTAR + "\n" + 'RENDU')
    elif newSTAR in listOfSTAR3:
      ultimateSTAR = (newslicedSTAR + "\n" + newSTAR + "\n" + 'ELVUM')
    elif newSTAR in listOfSTAR4:
      ultimateSTAR = (newslicedSTAR + "\n" + newSTAR + "\n" + 'VALUX')

    ADES = df.ADES[i]
    ADEP = df.ADEP[i]
    K = ("\u2193" +' '+ "F100")
    data= [[SSR, '', TYPE, ultimateSTAR, '', ADEP, K , 'L', 'N', 'P','R','T',],
          [CALLS, '', '', '', '', ADES, '' , '', '', '','','',],
          [WTC, 'E', mach_item, '', '',  'J', '' , 'M', 'O', 'Q','S','',]]

    t=Table(data,12*[0.66574803*inch], 3*[0.326772*inch])

    t.setStyle(TableStyle([
    ('SPAN',(0,1),(2,1)),
    ('SPAN',(3,0),(4,2)),
    ('SPAN',(6,0),(6,2)),
    ('SPAN',(7,0),(7,1)),
    ('SPAN',(8,0),(8,1)),
    ('SPAN',(9,0),(9,1)),
    ('SPAN',(10,0),(10,1)),
    ('SPAN',(11,0),(11,2)),

    ('ALIGN',(0,1),(2,1),'CENTER'),
    ('ALIGN',(5,0),(5,2),'CENTER'),
    ('VALIGN',(6,0),(6,2),'TOP'),
    ('ALIGN',(6,0),(6,2),'CENTER'),
    ('VALIGN',(11,0),(11,2),'TOP'),
    ('VALIGN',(3,0),(4,2),'TOP'),

    ('FONT',(0,1),(0,1),'Helvetica-Bold'),
    ('FONT',(6,0),(6,2),'Helvetica-Bold'),

    ('BOX', (0,0), (-1,-1), 2, colors.black),
    ('BOX', (3,0), (-1,-1), 0.25, colors.black),
    ('BOX', (5,0), (-1,-1), 0.25, colors.black),
    ('BOX', (6,0), (-1,-1), 0.25, colors.black),
    ('BOX', (7,0), (-1,-1), 0.25, colors.black),
    
    ('INNERGRID', (7,0), (-1,-1), 0.25, colors.black),
    ]))

    elements.append(t)

  elif df.ADEP[i] == 'BIKF':
    CALLS = df.CALLS[i]
    SSR = (df.SSR[i])
    TYPE = (df.TYPE[i])
    WTC = (df.WTC[i])

    mach_list = ['M079', 'M080' ,'M081' ,'M082' ]
    mach_item = random.choice(mach_list)

    oldSID = (df.SID[i])
    
    listSid = list(oldSID) 
    listSid.append(listSid.pop(listSid.index(listSid[0])))
    listSid.append(listSid.pop(listSid.index(listSid[0])))
    s = ''
    newSID = (s.join(listSid))

    oldROUTE_NAME = df.ROUTE_NAME[i]
    newROUTE_NAME = oldROUTE_NAME[3:5]

    oldTIME = df.TIME[i]
    listTIME = list(oldTIME)
    del listTIME[2]
    youngTIME = listTIME[0:4]
    s = ''
    newTIME = (s.join(youngTIME))

    RFL =  df.RFL[i]
    stringRFL = str(RFL)
    finalRFL = 'F'+ stringRFL

    POINT1 = df.POINTi[i]
    POINT2 = df['POINTi.1'][i]
    ROF = POINT1 +' '+  POINT2

    ADEP = df.ADEP[i]
    ADES = df.ADES[i]
    
    data= [[SSR, '', TYPE, newSID, '', '', finalRFL , ROF, '', ADEP],
          [ CALLS, '', '', newROUTE_NAME, newTIME, 'K', 'M', '', '', ADES,],
          [WTC, 'E', mach_item, '', 'I', '', '', '', '', 'P',]]

    t=Table(data,10*[0.7992126*inch], 3*[0.326772*inch])

    t.setStyle(TableStyle([
    ('SPAN',(0,1),(2,1)),
    ('SPAN',(3,0),(5,0)),
    ('SPAN',(3,1),(3,2)),
    ('SPAN',(5,1),(5,2)),
    ('SPAN',(6,1),(6,2)),
    ('SPAN',(7,0),(8,2)),
    
    ('FONT',(0,1),(0,1),'Helvetica-Bold'),

    ('ALIGN',(0,1),(2,1),'CENTER'),
    ('VALIGN',(3,1),(3,2),'MIDDLE'),
    ('VALIGN',(5,1),(5,2),'MIDDLE'),
    ('VALIGN',(6,1),(6,2),'TOP'),
    ('ALIGN',(6,0),(6,0),'RIGHT'),
    ('ALIGN',(6,1),(7,2),'CENTER'),
    ('VALIGN',(6,0),(6,0),'TOP'),
    ('VALIGN',(7,0),(8,2),'TOP'),
    ('VALIGN',(9,1),(9,1),'TOP'),
    ('FONTSIZE',(9,0),(9,0),7),

    ('BOX', (3,0), (5,3), 0.25, colors.black),
    ('BOX', (0,0), (-1,-1), 2, colors.black),
    ('BOX', (6,0), (6,3), 0.25, colors.black),
    ('BOX', (9,0), (-1,-1), 0.25, colors.black),
    ('INNERGRID', (3,0), (5,3), 0.25, colors.black),  
    #('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
    ]))

    elements1.append(t)

doc.build(elements)
doc1.build(elements1)
