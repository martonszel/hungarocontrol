# Hungarocontrol 

- Simple app for creating strips from a CSV file

## Getting Started

- git clone (https://gitlab.com/martonszel/hungarocontrol.git)
- cd hungarocontrol/

## Prerequisites

Step 1 

- To install Python, navigate to the Downloads Section of the [Python’ website](https://www.python.org/), download and install the Installer, best supported by your OS.

Step 2 
- `pip install -r requirements.txt`

Step 3 
- `python createstrips.py`

## Example 
 
![Example](img/strips.PNG)

## Built With
- Python Pandas & Reportlab 

